# flash

#### 项目介绍
激战长空！
这个项目的源代码来自于Github上的一个开源项目，由于现在登录Github不方便，关于开源协议究竟是什么暂时无法找到。暂时根据需要定为
GPL3.0。如有问题请联系我。现在我将这个项目作为软件工程课程的作业，希望学生可以在这个项目的基础上设计和开发出更加完整和好玩的
游戏，同时提升学生的软件工程能力。


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)